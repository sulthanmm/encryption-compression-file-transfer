﻿using System;  
using System.Collections.Generic;  
using System.Net;  
using System.Net.Sockets;  
using System.IO;  
using System.IO.Compression;
using System.Text; 
using System.Security.Cryptography;
using System.Runtime.InteropServices;
 
namespace EncryptionClient  
{  
    class Program  
    {  
        static void Main(string[] args)  
        {  
            try  
            {  
               	string filePath = @"D:\Jarkom\Compression Encryption\Client";
               	string fileName = "Test.exe";
               	string fileFullPath = filePath + @"\" + fileName;
                
                TcpClient tcpClient = new TcpClient("127.0.0.1", 8080);

                Console.WriteLine("Enter password : ");
                string password = Console.ReadLine();
 				
                StreamWriter sWriter = new StreamWriter(tcpClient.GetStream()); 
                
 	            FileEncrypt(fileFullPath, password);
 	            
 	            fileFullPath += ".aes";
                
 	            string compressionPath = @"D:\Jarkom\Compression Encryption\Client\Test.zip";
                ZipFile.CreateFromDirectory(filePath, compressionPath);
                byte[] bytes = File.ReadAllBytes(compressionPath);
                
                
                sWriter.WriteLine(bytes.Length.ToString());  
                sWriter.Flush();  
 				
                sWriter.WriteLine(compressionPath);  
                sWriter.Flush();  
                
                sWriter.WriteLine(password);
                sWriter.Flush();
 
                Console.WriteLine("Sending file");  
                tcpClient.Client.SendFile(compressionPath);  
 
            }  
            catch (Exception e)  
            {  
                Console.Write(e.Message);  
            }  
 
            Console.Read();  
        }  
        
        public static byte[] GenerateRandomSalt(){
		    byte[] data = new byte[32];
		
		    using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
		    {
		        for (int i = 0; i < 10; i++)
		        {
		            // Fille the buffer with the generated data
		            rng.GetBytes(data);
		        }
		    }
		
		    return data;
		}
        
        private static void FileEncrypt(string inputFile, string password){
		
		    //generate random salt
		    byte[] salt = GenerateRandomSalt();
		
		    //create output file name
		    FileStream fsCrypt = new FileStream(inputFile + ".aes", FileMode.Create);
		
		    //convert password string to byte arrray
		    byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);
		
		    //Set Rijndael symmetric encryption algorithm
		    RijndaelManaged AES = new RijndaelManaged();
		    AES.KeySize = 256;
		    AES.BlockSize = 128;
		    AES.Padding = PaddingMode.PKCS7;
		
		    var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
		    AES.Key = key.GetBytes(AES.KeySize / 8);
		    AES.IV = key.GetBytes(AES.BlockSize / 8);
	
		    AES.Mode = CipherMode.CFB;
		
		    // write salt to the begining of the output file, so in this case can be random every time
		    fsCrypt.Write(salt, 0, salt.Length);
		
		    CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateEncryptor(), CryptoStreamMode.Write);
		
		    FileStream fsIn = new FileStream(inputFile, FileMode.Open);
		
		    //create a buffer (1mb) so only this amount will allocate in the memory and not the whole file
		    byte[] buffer = new byte[1048576];
		    int read;
		
		    try
		    {
		        while ((read = fsIn.Read(buffer, 0, buffer.Length)) > 0)
		        {
		            cs.Write(buffer, 0, read);
		        }
		        Console.WriteLine("File Encrypted");
		        // Close up
		        fsIn.Close();
		    }
		    catch (Exception ex)
		    {
		        Console.WriteLine("Error: " + ex.Message);
		    }
		    finally
		    {
		        cs.Close();
		        fsCrypt.Close();
		    }
		}
    }  
} 