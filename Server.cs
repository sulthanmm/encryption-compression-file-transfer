﻿using System;  
using System.Collections.Generic;  
using System.Net;  
using System.Net.Sockets;  
using System.IO;  
using System.IO.Compression;
using System.Text;  
using System.Security.Cryptography;
using System.Runtime.InteropServices;
 
namespace EncryptionServer  
{  
    class Program  
    {  
        static void Main(string[] args)  
        {  
            // Listen on port 1234    
            TcpListener tcpListener = new TcpListener(IPAddress.Any, 8080);  
            tcpListener.Start();  
 
            Console.WriteLine("Server started");  
 
            //Infinite loop to connect to new clients    
            while (true)  
            {  
                // Accept a TcpClient    
                TcpClient tcpClient = tcpListener.AcceptTcpClient();  
 
                Console.WriteLine("Connected to client");  
 
                StreamReader reader = new StreamReader(tcpClient.GetStream());  
 
                // The first message from the client is the file size    
                string cmdFileSize = reader.ReadLine();  
 
                // The first message from the client is the filename    
                string cmdFileName = reader.ReadLine();  
                
                string password = reader.ReadLine();
 
                int length = Convert.ToInt32(cmdFileSize);  
                byte[] buffer = new byte[length];  
                int received = 0;  
                int read = 0;  
                int size = 4096;  
                int remaining = 0;  
 
                // Read bytes from the client using the length sent from the client    
                while (received < length)  
                {  
                    remaining = length - received;  
                    if (remaining < size)  
                    {  
                        size = remaining;  
                    }  
 
                    read = tcpClient.GetStream().Read(buffer, received, size);  
                    received += read;  
                }  
                
                
                
                
 
                // Save the file using the filename sent by the client    
                using (FileStream fStream = new FileStream(Path.GetFileName(cmdFileName), FileMode.Create))  
                {  
                    fStream.Write(buffer, 0, buffer.Length);
					

                    
                    fStream.Flush();  
                    fStream.Close();  
                }  
 				
                Console.WriteLine("File received and saved in " + Environment.CurrentDirectory);  
                ZipFile.ExtractToDirectory(Environment.CurrentDirectory + @"\test.zip", Environment.CurrentDirectory);
                FileDecrypt(Environment.CurrentDirectory + @"\test.exe.aes",Environment.CurrentDirectory + @"\test.exe", password);
                
            }  
        }  
        
        private static void FileDecrypt(string inputFile, string outputFile, string password) {
		    byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);
		    byte[] salt = new byte[32];
		
		    FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);
		    fsCrypt.Read(salt, 0, salt.Length);
		
		    RijndaelManaged AES = new RijndaelManaged();
		    AES.KeySize = 256;
		    AES.BlockSize = 128;
		    var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
		    AES.Key = key.GetBytes(AES.KeySize / 8);
		    AES.IV = key.GetBytes(AES.BlockSize / 8);
		    AES.Padding = PaddingMode.PKCS7;
		    AES.Mode = CipherMode.CFB;
		
		    CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateDecryptor(), CryptoStreamMode.Read);
		
		    FileStream fsOut = new FileStream(outputFile, FileMode.Create);
		
		    int read;
		    byte[] buffer = new byte[1048576];
		
		    try
		    {
		        while ((read = cs.Read(buffer, 0, buffer.Length)) > 0)
		        {
		         
		            fsOut.Write(buffer, 0, read);
		        }
		    }
		    catch (CryptographicException ex_CryptographicException)
		    {
		        Console.WriteLine("CryptographicException error: " + ex_CryptographicException.Message);
		    }
		    catch (Exception ex)
		    {
		        Console.WriteLine("Error: " + ex.Message);
		    }
		
		    try
		    {
		        cs.Close();
		    }
		    catch (Exception ex)
		    {
		        Console.WriteLine("Error by closing CryptoStream: " + ex.Message);
		    }
		    finally
		    {
		        fsOut.Close();
		        fsCrypt.Close();
		    }
		}
    }  
} 